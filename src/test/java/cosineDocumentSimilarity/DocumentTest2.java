package cosineDocumentSimilarity;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DocumentTest2 {

	private static Document alice;
	private static Document annaKarenina;
	private static Document anonyit;
	private static Document catalog1;
	private static Document catalog2;
	private static Document catalog3;
	private static Document redBadge;
	private static Document supercali;
	private static List<Document> docList;

	@BeforeClass
	public static void initDocs() throws IOException, Exception {
		alice = new Document(new File("testFiles/alice.txt").toURI().toURL().toString());
		annaKarenina = new Document(new File("testFiles/anna_karenina.txt").toURI().toURL().toString());
		anonyit = new Document(new File("testFiles/anonyit.txt").toURI().toURL().toString());
		catalog1 = new Document(new File("testFiles/catalog1.txt").toURI().toURL().toString());
		catalog2 = new Document(new File("testFiles/catalog2.txt").toURI().toURL().toString());
		catalog3 = new Document(new File("testFiles/catalog3.txt").toURI().toURL().toString());
		redBadge = new Document(new File("testFiles/red_badge.txt").toURI().toURL().toString());
		supercali = new Document(
				new File("testFiles/supercalifragilisticexpialidocious.txt").toURI().toURL().toString());
		docList = Arrays.asList(alice, annaKarenina, anonyit, catalog1, catalog2, catalog3, redBadge, supercali);
	}

	// tests for similarity between two documents
	// when the similarity is high
	@Test(timeout=40000)
	public void test1() throws IOException, Exception {
		try {
			assertEquals(98, catalog2.cosineSimilarity(catalog3));
			assertEquals(98, catalog3.cosineSimilarity(catalog2));
		} catch (Exception e) {
			fail("No exception expected!");
		}
	}

	// tests for similarity between two documents
	// when the similarity is moderate
	@Test(timeout=40000)
	public void test2() throws IOException, Exception {
		try {
			assertEquals(54, anonyit.cosineSimilarity(catalog2));
			assertEquals(54, catalog2.cosineSimilarity(anonyit));
		} catch (Exception e) {
			fail("No exception expected!");
		}
	}

	// tests for similarity between two long documents
	// when the similarity is high
	@Test(timeout=40000)
	public void test3() throws IOException, Exception {
		try {
			assertEquals(89, annaKarenina.cosineSimilarity(redBadge));
			assertEquals(89, redBadge.cosineSimilarity(annaKarenina));
		} catch (Exception e) {
			fail("No exception expected!");
		}
	}

	// tests for similarity between two long documents
	// when the similarity is low
	@Test(timeout=40000)
	public void test4() throws IOException, Exception {
		try {
			assertEquals(39, catalog2.cosineSimilarity(redBadge));
			assertEquals(39, redBadge.cosineSimilarity(catalog2));
		} catch (Exception e) {
			fail("No exception expected!");
		}
	}

	// tests the similarity between two empty documents
	// **empty and not null**
	// this would have a 0/0 computation
	// but no exception should be thrown
	// a return value of 0 or 100 is okay
	@Test(timeout=40000)
	public void test5() throws IOException, Exception {
		try {
			String url1 = new File("testFiles/blank1.txt").toURI().toURL().toString();
			String url2 = new File("testFiles/blank2.txt").toURI().toURL().toString();
			Document d1 = new Document(url1);
			Document d2 = new Document(url2);

			int csim100 = d1.cosineSimilarity(d2);
			boolean b = false;
			if (csim100 == 0 || csim100 == 100)
				b = true;
			assertEquals(true, b);
			assertEquals(d2.cosineSimilarity(d1), d1.cosineSimilarity(d2));
		} catch (Exception e) {
			fail("No exception expected!");
		}
	}

	// tests for similarity between two identical documents
	@Test(timeout=40000)
	public void test6() throws IOException, Exception {
		try {
			assertEquals(100, catalog2.cosineSimilarity(catalog2));
			assertEquals(100, catalog1.cosineSimilarity(catalog1));
		} catch (Exception e) {
			fail("No exception expected!");
		}
	}

	// tests for similarity between completely dissimilar documents
	@Test(timeout=40000)
	public void test7() throws IOException, Exception {
		try {
			assertEquals(0, supercali.cosineSimilarity(catalog1));
			assertEquals(0, catalog1.cosineSimilarity(supercali));
		} catch (Exception e) {
			fail("No exception expected!");
		}
	}

	// tests clustering
	// two clusters
	@Test(timeout=40000)
	public void test8() throws IOException, Exception {

		Map<Document, Integer> docGroups = DocumentSimilarity.groupSimilarDocuments(docList, 2);

		Set<Set<Document>> setOfSets2 = new HashSet<Set<Document>>();

		Set<Document> set1 = new HashSet<Document>();
		set1.add(supercali);

		Set<Document> set2 = new HashSet<Document>();
		set2.add(catalog1);
		set2.add(catalog2);
		set2.add(catalog3);
		set2.add(anonyit);
		set2.add(annaKarenina);
		set2.add(redBadge);
		set2.add(alice);

		setOfSets2.add(set1);
		setOfSets2.add(set2);

		for (Document d : docList) {
			setOfSets2.add(new HashSet<Document>());
		}

		List<Set<Document>> listOfSets = new ArrayList<Set<Document>>();

		for (int i = 0; i < docGroups.keySet().size(); i++)
			listOfSets.add(new HashSet<Document>());

		for (Document doc : docGroups.keySet()) {
			listOfSets.get(docGroups.get(doc)%docGroups.size()).add(doc);
		}

		Set<Set<Document>> setOfSets1 = new HashSet<Set<Document>>();
		for (Set<Document> s : listOfSets) {
			setOfSets1.add(s);
		}

		assertEquals(setOfSets2, setOfSets1);

	}

	// test clustering
	// three clusters
	@Test(timeout=40000)
	public void test9() throws IOException, Exception {
		Map<Document, Integer> docGroups = DocumentSimilarity.groupSimilarDocuments(docList, 3);
	
		Set<Set<Document>> setOfSets2 = new HashSet<Set<Document>>();

		Set<Document> set1 = new HashSet<Document>();
		set1.add(supercali);

		Set<Document> set2 = new HashSet<Document>();
		set2.add(catalog1);
		set2.add(catalog2);
		set2.add(catalog3);

		Set<Document> set3 = new HashSet<Document>();
		set3.add(anonyit);
		set3.add(annaKarenina);
		set3.add(redBadge);
		set3.add(alice);

		setOfSets2.add(set1);
		setOfSets2.add(set2);
		setOfSets2.add(set3);

		for (Document d : docList) {
			setOfSets2.add(new HashSet<Document>());
		}

		List<Set<Document>> listOfSets = new ArrayList<Set<Document>>();

		for (int i = 0; i < docGroups.keySet().size(); i++)
			listOfSets.add(new HashSet<Document>());

		for (Document doc : docGroups.keySet()) {
			listOfSets.get(docGroups.get(doc)%docGroups.size()).add(doc);
		}

		Set<Set<Document>> setOfSets1 = new HashSet<Set<Document>>();
		for (Set<Document> s : listOfSets) {
			setOfSets1.add(s);
		}

		assertEquals(setOfSets2, setOfSets1);

	}

	// test clustering
	// four clusters
	@Test(timeout=40000)
	public void test10() throws IOException, Exception {
		Map<Document, Integer> docGroups = DocumentSimilarity.groupSimilarDocuments(docList, 4);
		
		Set<Set<Document>> setOfSets2 = new HashSet<Set<Document>>();

		Set<Document> set1 = new HashSet<Document>();
		set1.add(supercali);

		Set<Document> set2 = new HashSet<Document>();
		set2.add(catalog2);
		set2.add(catalog3);

		Set<Document> set3 = new HashSet<Document>();
		set3.add(anonyit);
		set3.add(annaKarenina);
		set3.add(redBadge);
		set3.add(alice);

		Set<Document> set4 = new HashSet<Document>();
		set4.add(catalog1);

		setOfSets2.add(set1);
		setOfSets2.add(set2);
		setOfSets2.add(set3);
		setOfSets2.add(set4);

		for (Document d : docList) {
			setOfSets2.add(new HashSet<Document>());
		}

		List<Set<Document>> listOfSets = new ArrayList<Set<Document>>();

		for (int i = 0; i < docGroups.keySet().size(); i++)
			listOfSets.add(new HashSet<Document>());

		for (Document doc : docGroups.keySet()) {
			listOfSets.get(docGroups.get(doc)%docGroups.size()).add(doc);
		}

		Set<Set<Document>> setOfSets1 = new HashSet<Set<Document>>();
		for (Set<Document> s : listOfSets) {
			setOfSets1.add(s);
		}

		assertEquals(setOfSets2, setOfSets1);
	}

	// test clustering
	// number of clusters is one
	@Test(timeout=40000)
	public void test11() throws IOException, Exception {
		Map<Document, Integer> docGroups = DocumentSimilarity.groupSimilarDocuments(docList, 1);

		Set<Set<Document>> setOfSets2 = new HashSet<Set<Document>>();

		Set<Document> set1 = new HashSet<Document>();
		set1.add(supercali);
		set1.add(catalog2);
		set1.add(catalog3);
		set1.add(anonyit);
		set1.add(annaKarenina);
		set1.add(redBadge);
		set1.add(alice);
		set1.add(catalog1);

		setOfSets2.add(set1);

		for (Document d : docList) {
			setOfSets2.add(new HashSet<Document>());
		}

		List<Set<Document>> listOfSets = new ArrayList<Set<Document>>();

		for (int i = 0; i < docGroups.keySet().size(); i++)
			listOfSets.add(new HashSet<Document>());

		for (Document doc : docGroups.keySet()) {
			listOfSets.get(docGroups.get(doc)%docGroups.size()).add(doc);
		}

		Set<Set<Document>> setOfSets1 = new HashSet<Set<Document>>();
		for (Set<Document> s : listOfSets) {
			setOfSets1.add(s);
		}

		assertEquals(setOfSets2, setOfSets1);
	}

	// test clustering
	// number of clusters equals number of documents
	@Test(timeout=40000)
	public void test12() throws IOException, Exception {
		Map<Document, Integer> docGroups = DocumentSimilarity.groupSimilarDocuments(docList, 8);

		Set<Set<Document>> setOfSets2 = new HashSet<Set<Document>>();

		Set<Document> set1 = new HashSet<Document>();
		set1.add(supercali);
		Set<Document> set2 = new HashSet<Document>();
		set2.add(catalog2);
		Set<Document> set3 = new HashSet<Document>();
		set3.add(catalog3);
		Set<Document> set4 = new HashSet<Document>();
		set4.add(anonyit);
		Set<Document> set5 = new HashSet<Document>();
		set5.add(annaKarenina);
		Set<Document> set6 = new HashSet<Document>();
		set6.add(redBadge);
		Set<Document> set7 = new HashSet<Document>();
		set7.add(alice);
		Set<Document> set8 = new HashSet<Document>();
		set8.add(catalog1);

		setOfSets2.add(set1);
		setOfSets2.add(set2);
		setOfSets2.add(set3);
		setOfSets2.add(set4);
		setOfSets2.add(set5);
		setOfSets2.add(set6);
		setOfSets2.add(set7);
		setOfSets2.add(set8);

		List<Set<Document>> listOfSets = new ArrayList<Set<Document>>();

		for (int i = 0; i < docGroups.keySet().size(); i++)
			listOfSets.add(new HashSet<Document>());

		for (Document doc : docGroups.keySet()) {
			listOfSets.get(docGroups.get(doc)%docGroups.size()).add(doc);
		}

		Set<Set<Document>> setOfSets1 = new HashSet<Set<Document>>();
		for (Set<Document> s : listOfSets) {
			setOfSets1.add(s);
		}

		assertEquals(setOfSets2, setOfSets1);
	}

	// test clustering
	// too many clusters -- impossible!
	@Test(timeout=40000)
	public void test13() throws IOException, Exception {
		try {
			DocumentSimilarity.groupSimilarDocuments(docList, 10);
			fail("Should have seen an exception");
		} catch (Exception e) {
			// ho hum! nothing to do really...
		}
	}

}